import math
from tkinter import *
import time
from os import path

# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 20
reps = 0
timer = None


# ---------------------------- TIMER RESET ------------------------------- #
def reset_timer():
    global reps
    reps = 0
    window.after_cancel(timer)
    title.config(text="TIMER", fg=GREEN)
    count = 0
    time_remaining = time.strftime('%M:%S', time.gmtime(count))
    canvas.itemconfig(timer_text, text=time_remaining)
    mark = ""
    mile_stone_label.config(text=mark)


# ---------------------------- TIMER MECHANISM ------------------------------- #
def start_timer():
    global reps
    reps = reps + 1
    duration = 0
    if reps % 8 == 0:
        duration = LONG_BREAK_MIN
        title.config(text="BREAK", fg=RED)
    elif reps % 2 == 0:
        duration = SHORT_BREAK_MIN
        title.config(text="BREAK", fg=PINK)
    else:
        duration = WORK_MIN
        title.config(text="WORK", fg=GREEN)
    count_down(duration * 60)


# ---------------------------- COUNTDOWN MECHANISM ------------------------------- #
def count_down(count):
    time_remaining = time.strftime('%M:%S', time.gmtime(count))
    canvas.itemconfig(timer_text, text=time_remaining)

    if count > 0:
        global timer
        timer = window.after(1000, count_down, count - 1)
    else:
        start_timer()
        reps
        mark = ""
        checked = math.floor(reps / 2)
        for _ in range(checked):
            mark += "✔"
            mile_stone_label.config(text=mark)


# ---------------------------- UI SETUP ------------------------------- #

window = Tk()
window.title("Pomodoro")
window.config(padx=100, pady=50, bg=YELLOW)

# get icon
try:
    # Get the absolute path of the temp directory
    path_to_image = path.abspath(path.join(path.dirname(__file__), 'tomato.png'))

    path_to_icon= path.abspath(path.join(path.dirname(__file__), 'tomato.ico'))
    # set the icon of the tk window
    window.iconbitmap(path_to_icon)
except:
    pass

canvas = Canvas(width=200, height=224, bg=YELLOW, highlightthickness=0)
# tomato_img = PhotoImage(file="tomato.png")
tomato_img = PhotoImage(file=path_to_image)

canvas.create_image(100, 112, image=tomato_img)
timer_text = canvas.create_text(100, 130, text="00:00", fill="white", font=(FONT_NAME, 35, "bold"))
canvas.grid(column=1, row=1)

title = Label(text="Timer", fg=GREEN, bg=YELLOW, font=(FONT_NAME, 35, "bold"))
title.grid(column=1, row=0)

start_btn = Button(text="start", font=(FONT_NAME, 10, "bold"), highlightthickness=0, command=start_timer)
start_btn.grid(column=0, row=2)

reset_btn = Button(text="Reset", font=(FONT_NAME, 10, "bold"), highlightthickness=0, command=reset_timer)
reset_btn.grid(column=2, row=2)

mile_stone_label = Label(text="", fg=GREEN, bg=YELLOW, font=(FONT_NAME, 10, "bold"))
mile_stone_label.grid(column=1, row=3)

window.mainloop()

#https://stackoverflow.com/questions/48134269/python-pyinstaller-bundle-image-in-gui-to-onefile-exe
#C:\Users\cepar\Desktop\python-100\python-100-days\day-28>pyinstaller  --add-data=C:\Users\cepar\Desktop\python-100\python-100-days\day-28\tomato.ico;. --add-data=C:\Users\cepar\Desktop\python-100\python-100-days\day-28\tomato.png;. -F pomodoro.py --noconsole
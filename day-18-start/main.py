"""
import style 1
"""
from turtle import Turtle, Screen
import random

"""
import style 2
"""
# import turtle
# tim = turtle.Turtle

"""
import style 3 (bad pattern)
"""
# from turtle import *
# from random import *
# print(choice([1, 2, 3]))

"""
modules alias
"""
# import turtle as t
# tim = t.Turtle()

"""
how to install modules

see: https://pip.pypa.io/en/stable/user_guide/
"""

timmy_the_turtle = Turtle()
timmy_the_turtle.shape("turtle")
timmy_the_turtle.color("red")


def draw_dash(turtle, line_length):
    """
    :param turtle:
    :param line_length:
    :return:

    .. Turtle
        https://docs.python.org/3/library/turtle.html
    """
    for _ in range(round(line_length / (15 + 5))):
        turtle.forward(15)
        turtle.penup()
        turtle.forward(5)
        turtle.pendown()


# for i in range(4):
#     line_length = 100
#     draw_dash(timmy_the_turtle, line_length)
#     # timmy_the_turtle.forward(100)
#     timmy_the_turtle.right(90)

colours = ["CornflowerBlue", "DarkOrchid", "IndianRed", "DeepSkyBlue", "LightSeaGreen", "wheat", "SlateGray",
           "SeaGreen"]


def draw_polygon(turtle, number_of_side):
    angle = int(360 / number_of_side)

    for _ in range(number_of_side):
        # line_length = 5
        # draw_dash(timmy_the_turtle, line_length)
        turtle.forward(100)
        turtle.right(angle)


timmy_the_turtle = Turtle()

target_sides = 10

for number_of_side in range(3, target_sides + 1):
    timmy_the_turtle.color(random.choice(colours))
    draw_polygon(timmy_the_turtle, number_of_side)

screen = Screen()
screen.exitonclick()

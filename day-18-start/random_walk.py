import random
import turtle

colours = ["CornflowerBlue", "DarkOrchid", "IndianRed", "DeepSkyBlue", "LightSeaGreen", "wheat", "SlateGray",
           "SeaGreen"]

shapes = ["arrow", "turtle", "circle", "square", "triangle", "classic"]

tim = turtle.Turtle()
tim.shape("circle")
tim.color(random.choice(colours))
turtle.colormode(255)
tim.pensize(10)
tim.speed("fastest")


def get_random_color():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    return r, g, b


def random_walk(walk_length):
    for _ in range(walk_length):
        angle = [0, 90, 180, 270]
        tim.right(random.choice(angle))
        # tim.color(random.choice(colours))
        tim.color(get_random_color())
        tim.shape(random.choice(shapes))
        tim.forward(20)


random_walk(200)

screen = turtle.Screen()
screen.exitonclick()

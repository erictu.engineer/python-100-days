import colorgram
import random
import turtle

rgb_colors = []
colors = colorgram.extract('image.jpg', 30)

for color in colors:
    r, g, b = color.rgb
    # r = color.rgb.r
    # g = color.rgb.g
    # b = color.rgb.b

    rgb_colors.append((r, g, b))

color_list = rgb_colors
# print(color_list)


tim = turtle.Turtle()
tim.shape("classic")
turtle.colormode(255)
tim.color(random.choice(color_list))
tim.pensize(1)

tim.speed("fastest")


def draw_dash(turtle, line_length):
    for _ in range(1, line_length + 1):
        tim.color(random.choice(color_list))
        tim.begin_fill()
        tim.circle(10)
        tim.end_fill()
        tim.penup()
        tim.forward(30)


# x, y = tim.pos()
#
# print(x, y)
# draw_dash(tim, 10)
# tim.setposition(0, y + 40)
# draw_dash(tim, 10)

for _ in range(1, 10 + 1):
    x, y = tim.pos()
    draw_dash(tim, 10)
    tim.setposition(0, y + 30)

screen = turtle.Screen()
screen.exitonclick()

import colorgram
import random
import turtle

rgb_colors = []
colors = colorgram.extract('image.jpg', 30)

for color in colors:
    r, g, b = color.rgb
    # r = color.rgb.r
    # g = color.rgb.g
    # b = color.rgb.b

    rgb_colors.append((r, g, b))

color_list = rgb_colors

tim = turtle.Turtle()
tim.shape("classic")
turtle.colormode(255)
tim.color(random.choice(color_list))
tim.pensize(1)
tim.speed("fastest")
tim.penup()
tim.hideturtle()
tim.forward(300)
tim.setheading(0)
number_of_dots = 100

for dot_count in range(1, number_of_dots + 1):
    tim.dot(20, random.choice(color_list))
    tim.forward(50)

    if dot_count % 10 == 0:
        tim.setheading(90)
        tim.forward(50)
        tim.setheading(180)
        tim.forward(500)
        tim.setheading(0)

from turtle import Screen, Turtle
from paddle import Paddle
from ball import Ball
from score_board import ScoreBoard
import time

RIGHT_PADDLE_START_POSITION = (350, 0)
LEFT_PADDLE_START_POSITION = (-350, 0)
BALL_START_POSITION = (0, 0)


class Pong:
    def __init__(self):
        self.screen = self.create_screen()
        self.score_board = ScoreBoard()
        self.screen.tracer(0)
        self.right_paddle = Paddle(RIGHT_PADDLE_START_POSITION)
        self.left_paddle = Paddle(LEFT_PADDLE_START_POSITION)
        self.ball = Ball(BALL_START_POSITION)
        self.screen.update()

    def main(self):
        screen = self.screen
        ball = self.ball
        score_board = self.score_board
        right_paddle = self.right_paddle
        left_paddle = self.left_paddle
        screen.listen()

        screen.onkey(right_paddle.up, "Up")
        screen.onkey(right_paddle.down, "Down")
        screen.onkey(left_paddle.up, "w")
        screen.onkey(left_paddle.down, "s")

        score_board.countdown()
        score_board.refresh()
        is_game_on = True
        while is_game_on:
            screen.update()
            screen.tracer(1)
            ball = self.subscribe(ball, right_paddle, left_paddle, score_board)
            is_game_on = ball.move()
            screen.tracer(0)
        screen.exitonclick()

    def create_screen(self):
        screen = Screen()
        screen.setup(width=800, height=600)
        screen.bgcolor("black")
        screen.title("Pong")

        return screen

    def subscribe(self, ball, right_paddle, left_paddle, score_board):
        ball.right_paddle = right_paddle
        ball.left_paddle = left_paddle
        ball.score_board = score_board
        return ball


pong = Pong()
pong.main()

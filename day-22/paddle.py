from turtle import Screen, Turtle
import time

class Paddle(Turtle):
    def __init__(self, position):
        super(Paddle, self).__init__()
        self.shape('square')
        self.color('white')
        self.shapesize(5, 1)
        self.penup()
        self.goto(position)

    def up(self):
        x = self.xcor()
        y = self.ycor() + 20
        self.goto(x, y)

    def down(self):
        x = self.xcor()
        y = self.ycor() - 20
        self.goto(x, y)

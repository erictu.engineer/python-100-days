from turtle import Turtle
import time


class ScoreBoard(Turtle):
    def __init__(self):
        super().__init__()
        self.color("white")
        self.style = ('Courier', 15, 'italic')
        self.hideturtle()
        self.penup()
        self.goto(0, 250)
        self.left_score = 0
        self.right_score = 0
        self.write(f"Score: \n{self.left_score} : {self.right_score}", align="center", font=self.style)

    def increase_score(self, side):
        if side == 'left':
            self.left_score += 1
        if side == 'right':
            self.right_score += 1

        self.refresh()

    def refresh(self):
        self.clear()
        self.style = ('Courier', 15, 'italic')
        self.goto(0, 250)
        self.write(f"Score: \n{self.left_score} : {self.right_score}", align="center", font=self.style)

    def game_over(self):
        self.left_score = 0
        self.right_score = 0
        self.goto(0, 0)
        self.write("GAME OVER", align="center", font=self.style)
        time.sleep(1)

    def countdown(self):
        for i in range(3, 0, -1):
            self.goto(0, 0)
            self.style = ('Courier', 40, 'italic')
            self.write(f"{i}", align="center", font=self.style)
            time.sleep(1)
            self.refresh()
        self.goto(0, 250)

    def update_score(self, left_score, right_score):
        self.left_score = left_score
        self.right_score = right_score
        self.refresh()

    def is_game_over(self):
        if self.left_score >= 5:
            self.goto(0, 200)
            self.write("The winner is left side", align="center", font=self.style)
            return True
        if self.right_score >= 5:
            self.goto(0, 200)
            self.write("The winner is right side", align="center", font=self.style)
            return True
        return False

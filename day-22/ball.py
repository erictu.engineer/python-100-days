from turtle import Screen, Turtle


class Ball(Turtle):
    def __init__(self, position):
        super(Ball, self).__init__()
        self.shape('circle')
        self.color('white')
        self.penup()
        self.goto(position)
        self.right_paddle = None
        self.left_paddle = None
        self.score_board = None
        self.vector = {"x": 1, "y": 1}
        self.right_collision_times = 0
        self.left_collision_times = 0
        self.right_missing = 0
        self.left_missing = 0

    def move(self):
        slope = (600 - 20) / (800 - 20)
        shift_x = 1

        while True and not self.score_board.is_game_over():
            shift_y = shift_x * slope
            vector = self.vector
            x = self.xcor() + shift_x * vector["x"]
            y = self.ycor() + shift_y * vector["y"]

            if self.check_paddle_collisions():
                shift_x += 0.3

                left_score = self.left_collision_times + self.right_missing
                right_score = self.right_collision_times + self.left_missing
                self.score_board.update_score(left_score, right_score)
                x = self.xcor() - shift_x * vector["x"]
                y = self.ycor() - shift_y * vector["y"]
                self.vector["x"] *= -1

            if self.check_y_collisions():
                y = self.ycor() - shift_y * vector["y"]
                self.vector["y"] *= -1

            if self.check_x_collisions():
                left_score = self.left_collision_times + self.right_missing
                right_score = self.right_collision_times + self.left_missing
                self.score_board.update_score(left_score, right_score)
                self.reset()
                shift_x = 1
                continue
            self.goto(x, y)
        return False

    def check_paddle_collisions(self):
        if self.distance(self.right_paddle) <= 20:
            self.right_collision_times += 1
            return True
        if self.distance(self.left_paddle) <= 20:
            self.left_collision_times += 1
            return True
        return False

    def check_y_collisions(self):
        return abs(self.ycor()) > 280

    def check_x_collisions(self):
        if self.xcor() >= 390:
            self.right_missing += 1
            return True
        if self.xcor() <= -390:
            self.left_missing += 1
            return True
        return False

    def reset(self):
        self.color("black")
        self.setposition(0, 0)
        self.vector = {"x": 1, "y": 1}
        self.color("white")
        # self.right_collision_times = 0
        # self.left_collision_times = 0
        # self.right_missing = 0
        # self.left_missing = 0

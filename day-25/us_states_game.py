import pandas
import turtle

screen = turtle.Screen()
screen.title("U.S. States Game")
image = "blank_states_img.gif"
screen.addshape(image)
turtle.shape(image)

# def get_mouse_click_coor(x, y):
#     print(x, y)
#
#
# turtle.onscreenclick(get_mouse_click_coor)

correct_answer = 0
total_state = 50

# https://stackoverflow.com/questions/18695605/python-pandas-dataframe-to-dictionary
data = pandas.read_csv("50_states.csv")
states = data.set_index('state').T.to_dict()

missing_states = data.state.to_list()

correct_states = []


def update_map(state_name):
    pen = turtle.Turtle()
    pen.hideturtle()
    pen.penup()
    state = states[state_name]
    # position = (state.get("x"), state.get("y"))
    position = tuple(state.values())
    pen.goto(position)
    pen.write(state_name)


while correct_answer < total_state:
    answer_state = screen.textinput(title="Fuess the Stat", prompt="What's another state's name").title()

    if answer_state == "Exit":
        df = pandas.DataFrame(missing_states)
        df.to_csv("states_to_learn.csv")
        break

    if answer_state in states:
        update_map(answer_state)
        missing_states.remove(answer_state)
        correct_answer += 1
        screen.title(f"U.S. States Game {correct_answer} / {total_state}")

# states = pandas.read_csv("50_states.csv").to_dict()
# print(states)

# https://stackoverflow.com/questions/18012505/python-pandas-dataframe-columns-convert-to-dict-key-and-value
# states = pandas.read_csv("50_states.csv")
# states = dict(zip(states['state'], states['x']))
# print(states)

# states = pandas.read_csv("50_states.csv").T.to_dict().values()
# print(states)


turtle.mainloop()

# screen.exitonclick()

# states = pandas.read_csv("50_states.csv")

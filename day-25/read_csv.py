# with open("weather_data.csv") as data_file:
#     data = data_file.readlines()
#     print(data)

# import csv

# with open("weather_data.csv") as data_file:
#     data = csv.reader(data_file)
#     """
#     Skip the headers of the csv
#
#     Your reader variable is an iterable, by looping over it you retrieve the rows.
#     To make it skip one item before your loop, simply call next(reader, None) and ignore the return value.
#
#     ref: https://stackoverflow.com/questions/14257373/skip-the-headers-when-editing-a-csv-file-using-python
#     """
#     next(data, None)
#     temperatures = []
#     for row in data:
#         temperatures.append(int(row[1]))
#     print(temperatures)


import pandas
from functools import reduce
from statistics import mean
from math import isnan

data = pandas.read_csv("weather_data.csv")
print(type(data))  # DataFrame
print(type(data["temp"]))  # Series
print(data)
print(data["temp"])

data_dict = data.to_dict()
print(data_dict)
temp_list = data["temp"].to_list()
print(temp_list)


def average(lst):
    return mean(lst)
    # return sum(lst) / len(lst)
    # return reduce(lambda a, b: a + b, lst) / len(lst)


avg_temp = average(temp_list)

print(round(avg_temp, 2))

print(data["temp"].mean())
print(data["temp"].max())

# Get Data in Columns
print(data["condition"])
print(data.condition)

# Get Data in Row
print(data[data.day == "Monday"])

print(data[data.temp == data.temp.max()])

monday = data[data.day == "Monday"]
print(monday.condition)


def c2f(temp):
    return temp * 9 / 5 + 32


temp = int(monday.temp)
print(c2f(temp))

# Create a dataframe from scratch

data_dict = {
    "stuednts": ["Amy", "James", "Angela"],
    "scores": [76, 56, 65]
}

data = pandas.DataFrame(data_dict)
print(data)

data.to_csv("new_data.csv")

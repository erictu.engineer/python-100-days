import pandas

data = pandas.read_csv("2018_Central_Park_Squirrel_Census_-_Squirrel_Data.csv")

# fur_color_list = data["Primary Fur Color"].to_list()
fur_color_list = data[data["Primary Fur Color"].notnull()]["Primary Fur Color"].to_list()

fur_color_list_dist = list(set(fur_color_list))

fur_color_count_list = []

for color in fur_color_list_dist:
    fur_color_count_list.append(fur_color_list.count(color))

data_dict = {
    "Fur Color": fur_color_list_dist,
    "Count": fur_color_count_list,
}

data = pandas.DataFrame(data_dict)
data.to_csv("squirrel_count.csv")

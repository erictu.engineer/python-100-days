# TODO: Create a letter using starting_letter.txt
# for each name in invited_names.txt
# Replace the [name] placeholder with the actual name.
# Save the letters in the folder "ReadyToSend".

# Hint1: This method will help you: https://www.w3schools.com/python/ref_file_readlines.asp
# Hint2: This method will also help you: https://www.w3schools.com/python/ref_string_replace.asp
# Hint3: THis method will help you: https://www.w3schools.com/python/ref_string_strip.asp
import re


class MailBuilder:
    def __init__(self):
        self.template = ''
        self.receiver = ''

    def setTemplate(self, template):
        self.template = template

    def setReceiver(self, receiver):
        self.receiver = receiver

    def build(self):
        return re.sub(r"\[name\]", self.receiver, self.template)


mail_builder = MailBuilder()

with open("./Input/Letters/starting_letter.txt") as starting_letter:
    content = starting_letter.read()
    mail_builder.setTemplate(content)

invited_names = open("./Input/Names/invited_names.txt")
names = invited_names.readlines()

for name in names:
    name = name.strip('\n')
    mail_builder.setReceiver(name)
    mail = mail_builder.build()

    with open(f"./Output/ReadyToSend/mail_to_{name}.txt", mode="w") as file:
        file.write(mail)

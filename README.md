# Python 100 Days

[100 Days of Code - The Complete Python Pro Bootcamp for 2021](https://www.udemy.com/course/100-days-of-code)

- [x] Day 18 - Intermediate - Turtle & the Graphical User Interface (GUI)
- [x] Day 19 - Intermediate - Instances, State and Higher Order Functions
- [x] Day 20 - Intermediate - Build the Snake Game Part 1: Animation & Coordinates
- [x] Day 21 - Intermediate - Build the Snake Game Part 2: Inheritance & List Slicing
- [x] Day 22 - Intermediate - Build Pong: The Famous Arcade Game
- [x] Day 23 - Intermediate - The Turtle Crossing Capstone Project
- [x] Day 24 - Intermediate - Files, Directories and Paths
- [x] Day 25 - Intermediate - Working with CSV Data and the Pandas Library
- [x] Day 26 - Intermediate - List Comprehension and the NATO Alphabet
- [x] Day 27 - Intermediate - Tkinter, *args, **kwargs and Creating GUI Programs
- [x] Day 28 - Intermediate - Tkinter, Dynamic Typing and the Pomodoro GUI Application
- [ ] Day 29 - Intermediate - Building a Password Manager GUI App with Tkinter
- [ ] Day 30 - Intermediate - Errors, Exceptions and JSON Data: Improving the Password
- [ ] Day 31 - Intermediate - Flash Card App Capstone Project
- [ ] Day 32 - Intermediate+ Send Email (smtplib) & Manage Dates(datetime)
- [ ] Day 33 - Intermediate+ API Endpoints & API Parameters - ISS Overhead Notifier
- [ ] Day 34 - Intermediate+ API Practice - Creating a GUI Quiz App
- [ ] Day 35 - Intermediate+ Keys, Authentication & Environment Variables: Send SMS
from tkinter import *

window = Tk()
window.title("Mile to Km Converter")
window.minsize(width=500, height=300)
# #padding
window.config(padx=20, pady=20)

input = Entry(width=10)
input.grid(column=1, row=0)

my_label = Label(text="Miles", font=("Arial", 24, "bold"))
my_label.grid(column=2, row=0)
# my_label.config(padx=20, pady=20)

answer = 0
my_label = Label(text=f"is equal to {answer} Km", font=("Arial", 24, "bold"))
my_label.grid(column=1, row=1)


def calculate():
    miles = float(input.get())
    km = miles * 1.609344
    my_label.config(text=f"is equal to {km} Km", font=("Arial", 24, "bold"))


btn1 = Button(text="Calculate", command=calculate)
btn1.grid(column=1, row=2)

window.mainloop()

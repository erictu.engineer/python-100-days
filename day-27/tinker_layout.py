from tkinter import *

window = Tk()
window.title("My First GUI Program")
window.minsize(width=500, height=300)
# #padding
window.config(padx=20, pady=20)

my_label = Label(text="I Am a Label", font=("Arial", 24, "bold"))
my_label.config(text="New Text")
# my_label.pack(side="left")
# my_label.place(x=0, y=0)
# my_label.pack()
my_label.grid(column=0, row=0)
# my_label.config(padx=20, pady=20)

btn1 = Button(text="Btn1")
# btn1.pack()
btn1.grid(column=1, row=1)

btn2 = Button(text="Btn2")
# btn2.pack()
btn2.grid(column=2, row=0)

input = Entry(width=10)
# input.pack()

input.grid(column=3, row=3)

window.mainloop()

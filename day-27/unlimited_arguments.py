def add(n1, n2):
    return n1 + n2


add(n1=5, n2=3)


# unlimited arguments
def add(*args):
    print(args[0])
    print(type(args))  # tuple
    return sum(args)
    # answer = 0
    # for n in args:
    #     answer += n
    # return answer


result = add(3, 5, 6, 8)
print(result)

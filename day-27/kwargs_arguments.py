# many keyworded arguments

def calculate(**kwargs):
    print(type(kwargs))  # dictionary
    # for key, value in kwargs.items():
    #     print(key)
    #     print(value)
    print(kwargs["add"])
    print(kwargs["multiply"])


calculate(add=3, multiply=5)


def calculate(n, **kwargs):
    print(type(kwargs))  # dictionary
    # for key, value in kwargs.items():
    #     print(key)
    #     print(value)
    print(kwargs["add"])
    n += kwargs["add"]
    print(kwargs["multiply"])
    n *= kwargs["multiply"]
    print(n)


calculate(2, add=3, multiply=5)


class Car:

    def __init__(self, **kw):
        self.make = kw["make"]
        self.model = kw["model"]


# my_car = Car(make="Nissan")  # KeyError: 'model'


class Car:

    def __init__(self, **kw):
        self.make = kw.get("make")
        self.model = kw.get("model")
        self.color = kw.get("color")
        self.seats = kw.get("seats")


# the benefit of the get method is when get a not exist kw,
# it will return none instead of KeyError.


my_car = Car(make="Nissan", model="GT-R")
print(my_car.model)

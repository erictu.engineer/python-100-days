import re

sentence = "What is the Airspeed Velocity of an Unladen Swallow?"
sentence_list = sentence.split()
print(sentence_list)

sentence_list = [re.sub('\\W', '', sentence) for sentence in sentence_list]
print(sentence_list)

sentence_words = {sentence: len(sentence) for sentence in sentence_list}
print(sentence_words)

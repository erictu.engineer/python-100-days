def get_list_from_file(filename):
    with open(filename) as f:
        content = [int(i.strip()) for i in f.readlines()]
        return content


list1 = get_list_from_file("file1.txt")
list2 = get_list_from_file("file2.txt")
intersection = list(set(list1) & set(list2))

print(list1)
print(list2)
print(sorted(intersection))

intersection = [x for x in list1 if x in list2]
print(sorted(intersection))

intersection = set(list1).intersection(list2)
print(sorted(intersection))

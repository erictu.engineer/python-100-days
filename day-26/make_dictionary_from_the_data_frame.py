import pandas

df = pandas.read_csv("nato_phonetic_alphabet.csv")

phonetic_dict = {row.letter: row.code for (index, row) in df.iterrows()}
print(phonetic_dict)

word = input("please enter a word.").upper()
print(word)
phonetic_codes = [phonetic_dict[letter] for letter in list(word)]
print(phonetic_codes)

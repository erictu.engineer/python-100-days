numbers = [1, 2, 3]

new_list = []
for n in numbers:
    add_1 = n + 1
    new_list.append(add_1)
print(new_list)

# new_list = [new_item for item in list]

new_list = [n + 1 for n in numbers]
print(new_list)

name = "Eric"

letters_list = [letter for letter in name]

double_numbers = [n * 2 for n in range(1, 5)]
print(double_numbers)

double_numbers = [n * 2 for n in range(1, 5) if n % 2 == 0]
print(double_numbers)

names = ["Alex", "Beth", "Caroline", "Freddie"]

short_names = [name.upper() for name in names if len(name) < 5]
print(short_names)

squared_list = [n ** 2 for n in range(1, 5)]
print(squared_list)

even_list = [n for n in range(1, 5) if n % 2 == 0]
print(even_list)

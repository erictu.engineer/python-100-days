import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)

player = Player()
level = 1

scoreboard = Scoreboard()


def is_arrive(y_position):
    return y_position >= 300 - 20


def next_round():
    global level
    player.restart()
    car_manager.next_level()
    level += 1
    scoreboard.renew_score(level)


def move_player():
    player.go()
    if is_arrive(player.ycor()):
        next_round()


screen.listen()
screen.onkey(move_player, "Up")
car_manager = CarManager()

game_is_on = True
while game_is_on:
    time.sleep(0.1)
    screen.update()
    car_manager.create_car()
    car_manager.move_cars()

    for car in car_manager.all_cars:
        if car.distance(player) < 20:
            game_is_on = False
            scoreboard.game_over()

screen.exitonclick()

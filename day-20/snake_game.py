"""
Build the snake game

1. create a snake body

2. move the snake

3. create snake food

4. detect collision with food

5. create a scoreboard

6. detect collision with wall

7. detect collision with tail4. detect collision with food

5. create a scoreboard

6. detect collision with wall

7. detect collision with tail4. detect collision with food

5. create a scoreboard

6. detect collision with wall

7. detect collision with tail4. detect collision with food

5. create a scoreboard

6. detect collision with wall

7. detect collision with tail
"""

from turtle import Screen
import time
from snake import Snake
from food import Food
from scoreboard import ScoreBoard

screen = Screen()
screen.setup(width=600, height=600)
screen.bgcolor("black")
screen.title("My Snake Game")
screen.tracer(0)

snake = Snake()
food = Food()

score_board = ScoreBoard()

screen.listen()
screen.onkey(snake.up, "Up")
screen.onkey(snake.down, "Down")
screen.onkey(snake.left, "Left")
screen.onkey(snake.right, "Right")

game_is_on = True
while game_is_on:
    screen.update()
    time.sleep(0.5)
    snake.move()

    if snake.head.distance(food) < 15:
        print("nom nom nom")
        positions = food.get_positions()
        snake.extend()
        food.refresh()
        score_board.increase_score()

    if abs(snake.head.xcor()) > 280 or abs(snake.head.ycor()) > 280:
        game_is_on = False
        score_board.game_over()

    for segment in snake.segments[1:-1]:
        # if segment == snake.head:
        #     pass
        if snake.head.distance(segment) < 10:
            game_is_on = False
            score_board.game_over()

screen.exitonclick()

k = ["a", "b", "c", "d"]
k_tuple = ("a", "b", "c", "d")

print(k[0:-1:2])

print(k[:-1])

print(k[1:])

print(k[::2])

print(k[::-1])

# result
# ['a', 'c']
# ['a', 'b', 'c']
# ['b', 'c', 'd']
# ['a', 'c']
# ['d', 'c', 'b', 'a']

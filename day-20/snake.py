from turtle import Screen, Turtle

UP = 90
DOWN = 270
LEFT = 180
RIGHT = 0

class Snake(object):
    def __init__(self):
        self.starting_positions = [(0, 0), (-20, 0), (-40, 0)]
        self.move_distance = 20
        self.segments = []
        self.make_body()
        self.head = self.segments[0]
        self.tail = self.segments[-1]

    def make_body(self):
        starting_positions = self.starting_positions

        for position in starting_positions:
            segment = self.make_segment()
            segment.goto(position)
            self.segments.append(segment)

        return self.segments

    def move(self):
        for seg_num in range(len(self.starting_positions) - 1, 0, -1):
            positions = self.get_segment_positions(self.segments[seg_num - 1])
            if positions is None:
                break
            self.segments[seg_num].goto(positions)

        head_segments = self.segments[0]
        head_segments.forward(self.move_distance)

    def make_segment(self, shape="square", color="white"):
        segment = Turtle(shape)
        segment.color(color)
        segment.penup()
        return segment

    def get_segment_positions(self, segment):
        return segment.xcor(), segment.ycor()

    # def extend(self, position):
    #     segment = self.make_segment()
    #     segment.goto(position)
    #     self.segments.append(segment)
    #     self.starting_positions.append(position)
    #     return self

    def extend(self):
        segment = self.make_segment()
        position = self.tail.position()
        segment.goto(position)
        self.starting_positions.append(position)
        self.segments.append(segment)

    def up(self):
        if self.head.heading() != DOWN:
            self.head.setheading(UP)

    def left(self):
        if self.head.heading() != RIGHT:
            self.head.setheading(LEFT)

    def right(self):
        if self.head.heading() != LEFT:
            self.head.setheading(RIGHT)

    def down(self):
        if self.head.heading() != UP:
            self.head.setheading(DOWN)

from turtle import Turtle
import os

class ScoreBoard(Turtle):
    def __init__(self):
        super().__init__()
        self.color("white")
        self.style = ('Courier', 15, 'italic')
        self.hideturtle()
        self.penup()
        self.goto(0, 260)
        self.score = 0
        self.high_score = 0
        self.get_high_score()
        self.write(f"Score: {self.score}, High Score: {self.high_score}", align="center", font=self.style)

    def increase_score(self):
        self.score += 1

        if self.score > self.high_score:
            self.high_score = self.score
            self.update_high_score()

        self.refresh()

    def refresh(self):
        self.clear()
        self.write(f"Score: {self.score}, High Score: {self.high_score}", align="center", font=self.style)

    def game_over(self):
        self.goto(0, 0)
        self.write("GAME OVER", align="center", font=self.style)

    def get_high_score(self):
        filepath = "./high_score.txt"
        self.high_score = 0
        if os.path.isfile(filepath) and os.stat(filepath).st_size != 0:
            file = open(filepath, mode="r")
            contents = file.read()
            print(contents)
            self.high_score = int(contents)
            file.close()
        else:
            file = open(filepath, mode="w")
            contents = file.write("0")
            file.close()

    def update_high_score(self):
        filepath = "./high_score.txt"
        file = open(filepath, mode="w")
        contents = file.write(f"{self.high_score}")
        file.close()